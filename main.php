<?php

include_once 'AbstractFormClass.php';
/*
 * Класс валидатор для поля юзернейма в базовом варианте, осуществляет проверку на количество символов
 * (минимальная длина также может задаваться при создании объекта такого класса)
 * Также проверяет допустимые символы (паттерн допустимых символов можно задавать при создании объекта)
 */
class UsernameValidator extends AbstractValidator {

    protected $length = 8;
    protected $username_pattern = '^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]$';

    public function ___construct($required = false, $length = NULL, $pattern = '') {
        if (($length != NULL ) && is_numeric($length) && ($length > 0)) {
            $this->length = $length;
        }
        $this->is_required = $required;
        if ($pattern != '') {
            $this->username_pattern = $pattern;
        }
    }

    public function validate($value) {
        if ($this->isRequired() && !isset($value)) {
            $this->message = 'Blank required field';
            return false;
        } else if (strlen($value) >= $this->length) {
            if (1 == preg_match($this->email_pattern, $value)){
                return true;
            } else {
                $this->message = 'Not valid username';
                return false;
            }
            return true;
        } else {
            $this->message = 'Too short username';
            return false;
        }
    }

}
/*
 * Класс валидатор для поля пароля в базовом варианте, осуществляет проверку на количество символов
 * (минимальная длина также может задаваться при создании объекта такого класса)
 * Также проверяет допустимые символы (паттерн допустимых символов можно задавать при создании объекта)
 */
class PasswordValidator extends AbstractValidator {

    protected $length = 8;
    protected $password_pattern = '^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]$';

    public function ___construct($required = false, $length = NULL, $pattern = '') {
        if (($length != NULL ) && is_numeric($length) && ($length > 0)) {
            $this->length = $length;
        }
        $this->is_required = $required;
        if ($pattern != '') {
            $this->password_pattern = $pattern;
        }

    }

    public function validate($value) {
        if ($this->isRequired() && !isset($value)) {
            $this->message = 'Blank required field';
            return false;
        } else if (strlen($value) >= $this->length) {
            if (1 == preg_match($this->email_pattern, $value)){
                return true;
            } else {
                $this->message = 'Not valid password';
                return false;
            }
            return true;
        } else {
            $this->message = 'Too short password';
            return false;
        }
    }

}
/*
 * Класс валидатор для поля e-mail в базовом варианте,
 * Также проверяет допустимый паттерн, который можно задавать при создании объекта
 */
class EmailValidator extends AbstractValidator {

    protected $email_pattern = '/^((([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1})|([0-9А-Яа-я]{1}[-0-9А-я\.]{1,}[0-9А-Яа-я]{1}))@([-0-9A-Za-z]{1,}\.){1,2}[-A-Za-z]{2,})$/u';

    public function ___construct($required = false, $pattern = '') {
        $this->is_required = $required;
        if ($pattern != '') {
            $this->email_pattern = $pattern;
        }
    }

    public function validate($value) {
        if ($this->isRequired() && !isset($value)) {
            $this->message = 'Blank required field';
            return false;
        } else {
            if (1 == preg_match($this->email_pattern, $value)){
                return true;
            } else {
                $this->message = 'Not valid e-mail';
                return false;
            }
        }
    }

}
/*
 * Класс для создания и обработки формы регистрации
 */
class RegisterForm extends AbstractFormClass {

    public function __construct($method = 'POST') {

        $passwordValidator = new PasswordValidator($required = true, 8);
        $this->addField('username', new UsernameValidator($required = true, 12));
        $this->addField('email', new EmailValidator($required = true));
        $this->addField('password', $passwordValidator);
        $this->addField('submit');
        $this->method = $method;

    }

    public function render() {
        //
    }

    public function processForm() {
        //
    }

}
/*
 * Класс для создания и обработки формы входа
 */
class loginForm extends AbstractFormClass {

    public function __construct($method = 'POST') {

        $passwordValidator = new PasswordValidator($required = true, 8);
        $this->addField('username', new UsernameValidator($required = true, 12));
        $this->addField('password', $passwordValidator);
        $this->addField('submit');
        $this->method = $method;

    }

    public function render() {
        //
    }

    public function processForm() {
        //
    }

}

