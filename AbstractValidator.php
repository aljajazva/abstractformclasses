<?php
/*
 * Абстрактный класс валидатора поля ввода.
 * Описывает проверку необходимого поля ввода
 * Возможность задания специальных правил, в зависимости от типа поля
 * Возможность специальных сообщений
 */
abstract class AbstractValidator
{
    protected $is_required = false;
    protected $message = '';

    abstract public function validate($value);

    public function isRequired() {
        return $this->is_required;
    }

    public function getMessage(){
        return $this->message;
    }

}