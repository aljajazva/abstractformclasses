<?php

include_once 'AbstractValidator.php';
include_once 'Field.php';

/*
 * Абстрактный класс для создания и обработки данных HTML формы.
 * Описывает возможности рендеринга, конструирования и валидации формы.
 */

abstract class AbstractFormClass{

    protected $method;
    protected $action;
    protected $fields = array();

    /*
     * Визуализация формы должна быть описана отдельно для каждого подкласса
     */
    abstract public function render();

    /*
     * Специфическое дествие формы должно быть описано отдельно для каждого подкласса
     */
    abstract public function processForm();

    protected function addField($type, $validator = NULL){
        $this->fields[$type] = new Field($type, $validator);//создание нового поля
    }

    protected function setValues($fields_values){
        foreach ($fields_values as $type => $value){
            $this->fields[$type]->setValue($value);
        }
    }

    protected function validateForm() {
        foreach ($this->fields as $field) {
            if (!$field->validate()) {
                return false;
            }
        }
        return true;
    }

    public function actionForm() {
        $this->setValues($_POST);
        if ($this->validateForm()) {
            $this->processForm();
        } else {
            return false;
        }
    }

}

