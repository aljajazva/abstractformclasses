<?php
/*
 * Класс для создания отдельного поля формы, с возможностью указания дополнительных аттрибутов,
 * а также доступа к аттрибутам и их значениям
 */

class Field
{
    protected $attributes;
    protected $type;
    protected $value;
    protected $validator;

    public function ___construct($type, $validator = NULL){
        $this->setType($type);
        if ($validator !== NULL) {
            $this->setValidator($validator);
        }
    }

    public function addAttribute($name, $value)
    {
        $this->attributes[$name] = $value;
    }
    public function getAttribute($name){
        return $this->attributes[$name];
    }

    public function setType($type){
        $this ->type = $type;
    }
    public function getType(){
        return $this->type;
    }

    public function setValue($value){
        $this->value = $value;
    }
    public function getValue(){
        return $this->value;
    }

    public function setValidator($validator){
        $this->validator = $validator;
    }

    public function getValidator() {
        return $this->validator;
    }

    public function validate() {
        if ($this->validator !== NULL) {
            return $this->validator->validate($this->value);
        } else {
            return true;
        }
    }

}